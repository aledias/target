<header>
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="menu left-wing">
			<ul>
				<li>Professores</li>
				<li>Monte seu curso</li>
			</ul>
		</div>
  		<div id="logo-holder">
  			<a href="#">
				<img src="{{ asset('images/target-conhecimento-aplicado.svg') }}">
  			</a>
  		</div>
  		<div class="menu right-wing">
			<ul>
				<li>Artigos</li>
				<li>Contato</li>
			</ul>
		</div>
	</nav>
	<!--
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<ul>
			<li>Professores</li>
			<li>Monte seu Curso</li>
			<li class="logo">
				<svg>
					<use id="header-holder" href="#header"/>
					<use id="maxlogo" 		href="#logoquadrado"/>
				</svg>
			</li>
			<li>Artigos</li>
			<li>Contato</li>
		</ul>
	</nav>
	<svg>
		<use id="logoquadrado" href="#logoquadrado" width="300"/>
	</svg>
	<svg>
	</svg> -->
</header>

<div class="header"></div>