<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Target</title>
	<link href="{!! asset('css/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
</head>
	@include('partials.svgdefs')
<body>

	@include('partials.header')
	@yield('content')

	<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
</body>
</html>