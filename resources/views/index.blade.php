@extends('layouts.default')

@section('content')
  <div id="site-content">
    <section class="section section-home">
      Some section
    </section>
    <section class="section section-about">
      Some section
    </section>
    <section class="section section-products">
      <div class="c-products">
        <div class="c-product__item c-product__item--1">
          <img src="https://dummyimage.com/200x200/16f0f0/000000" alt="Nome do serviço" class="c-product__icon">
        </div>
        <div class="c-product__item c-product__item--2">
          <img src="https://dummyimage.com/200x200/16f0f0/000000" alt="Nome do serviço" class="c-product__icon">
        </div>
        <div class="c-product__item c-product__item--3">
          <img src="https://dummyimage.com/200x200/16f0f0/000000" alt="Nome do serviço" class="c-product__icon">
        </div>
        <div class="c-product__item c-product__item--4">
          <img src="https://dummyimage.com/200x200/16f0f0/000000" alt="Nome do serviço" class="c-product__icon">
        </div>
      </div>
    </section>
    <section class="section section-contact">
      Some section
    </section>
  </div>

  <ul id="site-content-menu" class="c-sections-menu">
    <li data-menuanchor="home"><a href="#home" class="c-sections-menu__link">First section</a></li>
    <li data-menuanchor="about"><a href="#about" class="c-sections-menu__link">Second section</a></li>
    <li data-menuanchor="products"><a href="#products" class="c-sections-menu__link">Third section</a></li>
    <li data-menuanchor="contact"><a href="#contact" class="c-sections-menu__link">Fourth section</a></li>
  </ul>
@stop