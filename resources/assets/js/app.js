import fullpage from 'fullpage.js/dist/fullpage.min.js';

window.addEventListener('load', () => {

  var siteContent = new fullpage('#site-content', {
    anchors: ['home', 'about', 'products', 'contact'],
    menu: '#site-content-menu',
    autoScrolling: true,
    scrollHorizontally: true
  });

});

