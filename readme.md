# Instruções

Para baixar os arquivos necessários para rodar o seu site, execute os comandos:


```
- composer install
- npm install
```


Para rodar seu site:

```
- [apenas 1x] php artisan key:generate
- php artisan serve
- npm run watch
```

Usando git
```
git status = mostra os arquivos *editados*
git add . = adiciona ao commit todos os arquivos criados / modificados
git add -u = adiciona ao commit os arquivos modificados
git commit -m "Descrição do commit" 
git push = envia o commit para o repositório
git pull = atualiza os arquivos no seu computador (vindos do repositorio)
```

Sequencia de comandos git
```
status > add > commit > push
```